package event_store

import (
	"context"
	"github.com/EventStore/EventStore-Client-Go/esdb"
)

var Client *esdb.Client

func Connect(url string) (*esdb.Client, error) {
	settings, err := esdb.ParseConnectionString(url)

	if err != nil {
		return nil, err
	}

	return esdb.NewClient(settings)
}

func Subscribe(client *esdb.Client) (*esdb.Subscription, error) {
	return client.SubscribeToAll(context.Background(), esdb.SubscribeToAllOptions{
		Filter: &esdb.SubscriptionFilter{
			Type:     esdb.StreamFilterType,
			Prefixes: []string{"customer_"},
		},
	})
}
