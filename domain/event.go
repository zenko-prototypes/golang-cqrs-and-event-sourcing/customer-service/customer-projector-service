package domain

import (
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/google/uuid"
)

type Event interface {
	Name() string
	Handle(client *elasticsearch.Client) error
}

type BaseEvent struct {
	AggregateID uuid.UUID
}
