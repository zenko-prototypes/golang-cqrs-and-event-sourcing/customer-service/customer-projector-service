package domain

import (
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/google/uuid"
	"strings"
)

type CustomerCreated struct {
	BaseEvent
	CustomerID   uuid.UUID
	CustomerName string
}

func (e CustomerCreated) Handle(client *elasticsearch.Client) error {

	body := strings.NewReader(fmt.Sprintf(`{"name": "%s" }`, e.CustomerName))
	res, err := client.Create("customer", e.CustomerID.String(), body)
	if err != nil {
		return err
	}
	err = res.Body.Close()
	return err
}

func (e CustomerCreated) Name() string {
	return "CustomerCreated"
}
