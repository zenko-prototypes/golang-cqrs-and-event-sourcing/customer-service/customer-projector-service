package domain

import "github.com/elastic/go-elasticsearch/v7"

type CustomerDeleted struct {
	BaseEvent
}

func (e CustomerDeleted) Handle(client *elasticsearch.Client) error {
	res, err := client.Delete("customer", e.AggregateID.String())
	if err != nil {
		return err
	}
	err = res.Body.Close()
	return err
}

func (e CustomerDeleted) Name() string {
	return "CustomerDeleted"
}
