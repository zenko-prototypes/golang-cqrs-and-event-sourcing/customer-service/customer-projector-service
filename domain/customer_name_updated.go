package domain

import (
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"strings"
)

type CustomerNameUpdated struct {
	BaseEvent
	CustomerName string
}

func (e CustomerNameUpdated) Handle(client *elasticsearch.Client) error {
	res, err := client.Update("customer", e.AggregateID.String(), strings.NewReader(fmt.Sprintf(`{ "doc": { "name": "%s" }}`, e.CustomerName)))
	if err != nil {
		return err
	}
	err = res.Body.Close()
	return err
}

func (e CustomerNameUpdated) Name() string {
	return "CustomerNameUpdated"
}
