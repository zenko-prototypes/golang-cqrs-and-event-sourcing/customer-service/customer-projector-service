package main

import (
	"CustomerProjectorService/domain"
	"CustomerProjectorService/elasticsearch"
	"CustomerProjectorService/event_store_db"
	"encoding/json"
	"fmt"
	"github.com/EventStore/EventStore-Client-Go/esdb"
	"log"
)

func main() {
	c, err := event_store.Connect("esdb://localhost:2113?tls=false")
	if err != nil {
		log.Fatal(err)
	}

	sub, err := event_store.Subscribe(c)
	if err != nil {
		panic(err)
	}

	defer func(sub *esdb.Subscription) {
		err := sub.Close()
		if err != nil {

		}
	}(sub)

	es, err := elasticsearch.Connect()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	for {
		event := sub.Recv()

		if event.EventAppeared != nil {

			switch event.EventAppeared.Event.EventType {
			case "CustomerCreated":
				e := &domain.CustomerCreated{}
				err := json.Unmarshal(event.EventAppeared.Event.Data, e)
				if err != nil {
					log.Fatalf("Error when parsing the event: %s", err)
				}
				err = e.Handle(es)
				if err != nil {
					log.Fatalf("Error when handling the event: %s", err)
				}
				break
			case "CustomerNameUpdated":
				e := &domain.CustomerNameUpdated{}
				err := json.Unmarshal(event.EventAppeared.Event.Data, e)
				if err != nil {
					log.Fatalf("Error when parsing the event: %s", err)
				}
				err = e.Handle(es)
				if err != nil {
					log.Fatalf("Error when handling the event: %s", err)
				}
				break
			case "CustomerDeleted":
				e := &domain.CustomerDeleted{}
				err := json.Unmarshal(event.EventAppeared.Event.Data, e)
				if err != nil {
					log.Fatalf("Error when parsing the event: %s", err)
				}
				err = e.Handle(es)
				if err != nil {
					log.Fatalf("Error when handling the event: %s", err)
				}
				break
			default:
				fmt.Printf("Event not handled")
				break

			}
		}

		if event.SubscriptionDropped != nil {
			break
		}
	}
}
